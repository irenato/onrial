<?php
/*
Template name: about
*/
get_header('about'); ?>
	<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
		<div class="container">
			<?php do_action( 'woocommerce_before_main_content' ); ?>
		</div>
		<?php the_content(); ?>
	<?php endwhile; else : endif; ?>
	</div>
	</div>

<?php get_footer(); ?>