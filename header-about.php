<?php
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="utf-8">

    <title>Onrial - производство материалов для наращивания ресниц.</title>
    <meta name="description" content="">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <meta property="og:image" content="path/to/image.jpg">
    <link rel="shortcut icon" href="img/favicon/favicon.png" type="image/png">
    <!--    <link rel="stylesheet" href="/css/main.min.css">-->

    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#000">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#000">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#000">
    <!--    <style>body { opacity: 0; overflow-x: hidden; } html { background-color: #fff; }</style>-->

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<header class="header header-about">
    <div class="top-line">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <?php wp_nav_menu(array(
                        'menu' => 'headMenu',
                        'menu_class' => 'top-mnu',
                    )); ?>

                <div class="col-md-2 col-right">
                    <a href="/checkout/" class="cart-btn">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/ico-cart.png" alt="" class="">
                        <span class="cart-btn__amount"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
                        <span class="cart-btn__price"><?php echo WC()->cart->get_cart_subtotal(); ?></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="logo-line">
            <div class="row">
                <div class="col-xs-7 col-sm-6">
                    <a href="/" class="logo">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="">
                    </a>
                    <span class="main-descr">Производство качественных безопасных материалов для наращивания ресниц</span>
                </div>
                <div class="col-xs-5 col-right col-sm-6">
                    <a href="javascript:void(0)" data-mfp-src="#consult-popup" class="btn header-popup popup-btn">Обратный
                        звонок</a>
                    <ul class="header-phones">
                        <li>+38 (‎‎096) 600 01 55</li>
                        <li>+38 (050) 600 01 55</li>
                        <li>+38 (073) 600 01 55</li>
                    </ul>
                </div>
            </div>
        </div>
        <nav class="mnu-wrap clearfix">
            <?php wp_nav_menu(array(
                'menu' => 'mainMenu',
                'menu_class' => 'main-mnu',
            )); ?>
            <div class="search-form">
                <!-- <form role="search" method="get" id="searchform" class="searchform"
                      action="<?php echo esc_url( home_url( '/' ) ); ?>">
                    <div>
                        <label class="screen-reader-text" for="s"><?php echo _x( 'Search for:', 'label' ); ?></label>
						<input type="hidden" name="post_type" value="product">
                        <input type="text" class="search-input" value="<?php get_search_query() ?>" name="s" id="s"
                               placeholder="Поиск по сайту.."/>
                        <input type="submit" class="search-btn " id="searchsubmit"
                               value="<?php esc_attr_x('Search', 'submit button') ?>"/><img
                                src="<?php echo get_template_directory_uri(); ?>/img/ico-search.png" alt="">
                    </div>
                </form> -->
				<?php echo do_shortcode('[yith_woocommerce_ajax_search]'); ?> 

            </div>

        </nav>
		<div class="about-info">
			<h1>О компании</h1>
			<p>ONRIAL - Корейский производитель качественных безопасных <br> материалов для наращивания ресниц, одобренных ассоциацией дерматологов.</p>
		</div>
    </div>
</header>

