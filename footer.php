<?php

?>

<footer class="footer">
    <!--<div class="container">-->
    <!--<div class="footer-top">-->
    <!--<div class="footer-logo">-->
    <!--<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="">-->
    <!--<div class="footer-copyright">Copyright © 2017 onrial.com.ua</div>-->
    <!--</div>-->
    <!--</div>-->
    <!--<div class="footer-bottom clearfix">-->
    <!--<ul class="footer-mnu">-->
    <!--<li><a href="javascript:void(0)" class="footer-mnu__link">Главная</a></li>-->
    <!--<li><a href="javascript:void(0)" class="footer-mnu__link">Продукция</a></li>-->
    <!--<li><a href="javascript:void(0)" class="footer-mnu__link">Наше производство</a></li>-->
    <!--<li><a href="javascript:void(0)" class="footer-mnu__link">Фотогаллерея</a></li>-->
    <!--<li><a href="javascript:void(0)" class="footer-mnu__link">Новости</a></li>-->
    <!--<li><a href="javascript:void(0)" class="footer-mnu__link">Контакты</a></li>-->
    <!--</ul>-->
    <!--<form action="" class="footer-subscribe">-->
    <!--<input type="text" class="footer-subscribe__input" placeholder="Подписаться на рассылку">-->
    <!--<button class="footer-subscribe__btn">Отправить</button>-->
    <!--</form>-->
    <!--<ul class="footer-social">-->
    <!--<li><a href="javascript:void(0)" class="footer-social__link"><i>&#xf09a</i></a></li>-->
    <!--<li><a href="javascript:void(0)" class="footer-social__link"><i>&#xF16D</i></a></li>-->
    <!--<li><a href="javascript:void(0)" class="footer-social__link"><i>&#xF16A</i></a></li>-->
    <!--<li><a href="javascript:void(0)" class="footer-social__link"><i>&#xF0D5</i></a></li>				-->
    <!--</ul>-->
    <!--</div>-->
    <!--</div>-->
    <div class="container">
        <div class="logo-line">
            <div class="row">
                <div class="col-xs-7 col-sm-6">
                    <a href="/" class="logo">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="">
                    </a>
                    <span class="main-descr">Производство качественных безопасных материалов для наращивания ресниц</span>
                </div>
                <div class="col-xs-5 col-right col-sm-6">
                    <a href="javascript:void(0)" data-mfp-src="#consult-popup" class="btn header-popup popup-btn">Обратный звонок</a>
                    <ul class="header-phones">
                        <li>+38 (‎‎096) 600 01 55</li>
                        <li>+38 (050) 600 01 55</li>
                        <li>+38 (073) 600 01 55</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="mfp-hide consult-popup">
    <form id='consult-popup' action="" class="popup-form pbz_form clear-styles" data-error-title="Ошибка отправки!" data-error-message="Попробуйте отправить заявку через некоторое время." data-success-title="Спасибо за заявку!" data-success-message="Наш менеджер свяжется с вами в ближайшее время.">
        <input type="hidden" name="tagmanager" value="form.html">
        <!--<h3>Получить каталог</h3>-->
        <span>Введите информацию и наш менеджер перезвонит Вам в течение 10 минут</span>
        <ul class="popup-form-fields">
            <li class="popup-form-composition">
                <input type="text" name="name" class="popup-form-input" placeholder="Введите Имя" data-validate-required="Обязательное поле" data-title="Имя">
            </li>
            <li class="popup-form-composition">
                <input type="text" name="phone" class="popup-form-input" placeholder="Введите Ваш телефон" data-validate-required="Обязательное поле" data-validate-uaphone="Неправильный номер" data-title="Телефон">
            </li>
            <li class="popup-form-composition btn-composition">
                <button type="submit" class="popup-form-btn cost-btn">Отправить</button>
            </li>
        </ul>
    </form>

    <form id='prod-popup' action="" class="popup-form pbz_form clear-styles" data-error-title="Ошибка отправки!" data-error-message="Попробуйте отправить заявку через некоторое время." data-success-title="Спасибо за заявку!" data-success-message="Наш менеджер свяжется с вами в ближайшее время.">
            <div class="row flex">
                <div class="col-sm-7">
                    <img src="/wp-content/themes/onrial/img/popup_bg.png" alt="bg">
                </div>
                <div class="col-sm-5">
                    <div class="info">
                        <input type="hidden" name="tagmanager" value="form.html">
                        <h3>Благодарим за интерес!</h3>
                        <span>К сожалению, на данный момент данной группы
    товаров нет в наличии. Мы постоянно улучшаем
    качество нашей продукции, уделяем максимум
     внимания инновационным технологиям
    и использванию материалов нового поколения
    на каждом этапе производства.
                        </span>
                        <span><b>Для получения информации о сроках
    поступления новой партии товаров или
    предзаказа по спеццене оставьте заявку ниже</b></span>
                        <ul class="popup-form-fields">
                            <li class="popup-form-composition">
                                <input type="text" name="phone" class="popup-form-input" placeholder="Введите Ваш телефон" data-validate-required="Обязательное поле" data-validate-uaphone="Неправильный номер" data-title="Телефон">
                            </li>
                            <li class="popup-form-composition btn-composition">
                                <button type="submit" class="popup-form-btn cost-btn">Отправить</button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </form>
	
	<form id='prod-popup-old' action="" class="popup-form pbz_form clear-styles" data-error-title="Ошибка отправки!" data-error-message="Попробуйте отправить заявку через некоторое время." data-success-title="Спасибо за заявку!" data-success-message="Наш менеджер свяжется с вами в ближайшее время.">
        <input type="hidden" name="tagmanager" value="form.html">
        <!--<h3>Получить каталог</h3>-->
        <span>Введите информацию и получите скидку до 30%</span>
        <ul class="popup-form-fields">
            <li class="popup-form-composition">
                <input type="text" name="name" class="popup-form-input" placeholder="Введите Имя" data-validate-required="Обязательное поле" data-title="Имя">
            </li>
            <li class="popup-form-composition">
                <input type="text" name="phone" class="popup-form-input" placeholder="Введите Ваш телефон" data-validate-required="Обязательное поле" data-validate-uaphone="Неправильный номер" data-title="Телефон">
            </li>
            <li class="popup-form-composition btn-composition">
                <button type="submit" class="popup-form-btn cost-btn">получить скидку</button>
            </li>
        </ul>
    </form>
</div>

<!-- <script src="js/scripts.min.js"></script> -->
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAGRDDZ_fNeJQ_oCKwXglQoHl3-9J-gszE"></script> -->
<?php wp_footer(); ?>

</body>
</html>
