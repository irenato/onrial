<?php
add_action( 'wp_enqueue_scripts', function(){
//    wp_enqueue_script('jquery-min', get_template_directory_uri() . '/libs/jquery/dist/jquery.min.js');
    wp_enqueue_script('common-js', get_template_directory_uri() . '/js/common.js');
    //wp_enqueue_script('slick-js', get_template_directory_uri() . '/libs/slick/slick.min.js');
    //wp_enqueue_script('maskedinput-js', get_template_directory_uri() . '/libs/jquery.maskedinput/jquery.maskedinput.min.js');
    //wp_enqueue_script('magnific-js', get_template_directory_uri() . '/libs/magnific-popup/jquery.magnific-popup.min.js');
    //wp_enqueue_script('script-min', get_template_directory_uri() . '/js/scripts.min.js');
    wp_enqueue_style('min_css', get_template_directory_uri() . '/css/main.min.css');




    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/libs/bootstrap-grid/bootstrap-grid.css');
    wp_enqueue_style('magnific', get_template_directory_uri() . '/libs/magnific-popup/magnific-popup.css');
    wp_enqueue_style('sweetalert2', get_template_directory_uri() . '/libs/sweetalert2/sweetalert2.min.css');



    wp_enqueue_style('css', get_template_directory_uri() . '/style.css');

});

register_nav_menus(array(
    'top' => 'Верхнее меню'
));


function search_form_modify( $html ) {
    return str_replace( 'class="search-submit"', 'class="search-submit screen-reader-text"', $html );
}
add_filter( 'get_search_form', 'search_form_modify' );

add_theme_support('widgets');

function wp_sidebars() {

    register_sidebars(1,
        array('id' => 'products-sidebar',
            'name' => __('Боковая панель на странице товаров'),
            'description' => __( 'The first (primary) sidebar.'),
            'before_title' => '<h1>',
		    'after_title' => '</h1>'
        ));

}

add_action( 'widgets_init', 'wp_sidebars' );

add_filter( 'woocommerce_product_add_to_cart_text', 'woo_custom_product_add_to_cart_text' );  // 2.1 +

function woo_custom_product_add_to_cart_text() {

    return __( 'В корзину', 'woocommerce' );

}
add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_single_add_to_cart_text' );  // 2.1 +

function woo_single_add_to_cart_text() {

    return __( 'В корзину', 'woocommerce' );

}

remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );

//* Активируем поддержку шрифта Font Awesome
add_action( 'wp_enqueue_scripts', 'enqueue_font_awesome' );
function enqueue_font_awesome() {

    wp_enqueue_style( 'font-awesome', '//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css' );

}


//добавляем нужные кнопки в меню
add_filter('wp_nav_menu_items', 'add_login_logout_link', 10, 2);

function add_login_logout_link($items, $args) {
    if($args->menu == 'headMenu') {
//        $loginoutlink = wp_loginout('index.php', false);
        $items .= '</ul>';
        $items .= '</div>';
        $items .= '</div>';
        $items .= '<div class="col-md-2">';
        $items .= '    <ul class="top-mnu top-mnu&#45;&#45;login">';
        if (!is_user_logged_in())
            $items .= '<li><a href="/account/edit-account/">Регистрация</a></li>';
        else
            $items .= '<li><a href="/account/edit-account/">Кабинет</a></li>';
        if (is_user_logged_in() && $args->menu == 'headMenu') {

            $items .= '<li><a href="'. wp_logout_url( get_permalink( woocommerce_get_page_id( 'myaccount' ) ) ) .'">Выйти</a></li>';

        }

        elseif (!is_user_logged_in() && $args->menu == 'headMenu') {

            $items .= '<li><a href="' . get_permalink( woocommerce_get_page_id( 'myaccount' ) ) . '">Войти</a></li>';

        }
        $items .= '</ul>';

        return $items;
    }else {
        return $items;
    }
}

function wooc_extra_register_fields() {?>
   <div class="col-sm-6">
       <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
           <label for="reg_billing_first_name">Ваше имя <span class="required">*</span></label>
           <input type="text" class="input-text" name="billing_first_name" id="reg_billing_first_name" placeholder="Введите Ваше имя" value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" />
       </p>
    </div>
    <div class="col-sm-6">
        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
            <label for="billing_phone">Ваш телефон <span class="required">*</span></label>
            <input type="text" class="input-text" name="billing_phone" id="reg_billing_phone" placeholder="Введите Ваш телефон" value="<?php esc_attr_e( $_POST['billing_phone'] ); ?>" /></p>
    </div>
    <?php
}
add_action( 'woocommerce_register_form_start', 'wooc_extra_register_fields' );


function wooc_validate_extra_register_fields( $username, $email, $validation_errors ) {

    if ( isset( $_POST['billing_phone'] ) && empty( $_POST['billing_phone'] ) ) {

        $validation_errors->add( 'billing_phone_error', __( '<strong>Error</strong>: Phone is required!.', 'woocommerce' ) );

    }

     return $validation_errors;
}

add_action( 'woocommerce_register_post', 'wooc_validate_extra_register_fields', 10, 3 );



// Change number or products per row to 3
add_filter('loop_shop_columns', 'loop_columns');
if (!function_exists('loop_columns')) {
    function loop_columns() {
        return 2; // 3 products per row
    }
}
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );



add_filter('woocommerce_currency_symbol', 'add_my_currency_symbol', 10, 2);

function add_my_currency_symbol( $currency_symbol, $currency )
{

    switch ($currency) {

        case 'UAH':
            $currency_symbol = ' грн';
            break;

    }

    return $currency_symbol;
}


function my_acf_google_map_api( $api ){

    $api['key'] = 'AIzaSyC-A04z2hqlnoeOBciuiT7uBesuzCYgUTA';

    return $api;

}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

/* API Maps */
function my_theme_add_scripts() {
    ?>
    <script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyC-A04z2hqlnoeOBciuiT7uBesuzCYgUTA'></script>
    <?php
}
add_action( 'wp_enqueue_scripts', 'my_theme_add_scripts' );

if ( ! function_exists( 'wc_dropdown_variation_attribute_options' ) ) {

    /**
     * Output a list of variation attributes for use in the cart forms.
     *
     * @param array $args Arguments.
     * @since 2.4.0
     */
    function wc_dropdown_variation_attribute_options( $args = array() ) {
        $args = wp_parse_args( apply_filters( 'woocommerce_dropdown_variation_attribute_options_args', $args ), array(
            'options'          => false,
            'attribute'        => false,
            'product'          => false,
            'selected' 	       => false,
            'name'             => '',
            'id'               => '',
            'class'            => '',
            'show_option_none' => false,
        ) );

        $options               = $args['options'];
        $product               = $args['product'];
        $attribute             = $args['attribute'];
        $name                  = $args['name'] ? $args['name'] : 'attribute_' . sanitize_title( $attribute );
        $id                    = $args['id'] ? $args['id'] : sanitize_title( $attribute );
        $class                 = $args['class'];
        $show_option_none      = $args['show_option_none'] ? true : false;
        $show_option_none_text = $args['show_option_none'] ? $args['show_option_none'] : __( 'Choose an option', 'woocommerce' ); // We'll do our best to hide the placeholder, but we'll need to show something when resetting options.

        if ( empty( $options ) && ! empty( $product ) && ! empty( $attribute ) ) {
            $attributes = $product->get_variation_attributes();
            $options    = $attributes[ $attribute ];
        }

        $html = '<select id="' . esc_attr( $id ) . '" class="' . esc_attr( $class ) . '" name="' . esc_attr( $name ) . '" data-attribute_name="attribute_' . esc_attr( sanitize_title( $attribute ) ) . '" data-show_option_none="' . ( $show_option_none ? 'yes' : 'no' ) . '">';
        $html .= '<option value="">' . esc_html( $show_option_none_text ) . '</option>';

        if ( ! empty( $options ) ) {
            if ( $product && taxonomy_exists( $attribute ) ) {
                // Get terms if this is a taxonomy - ordered. We need the names too.
                $terms = wc_get_product_terms( $product->get_id(), $attribute, array( 'fields' => 'all' ) );

                foreach ( $terms as $term ) {
                    if ( in_array( $term->slug, $options ) ) {
                        $html .= '<option value="' . esc_attr( $term->slug ) . '" ' . selected( sanitize_title( $args['selected'] ), $term->slug, false ) . '>' . esc_html( apply_filters( 'woocommerce_variation_option_name', $term->name ) ) . '</option>';
                    }
                }
            } else {
                foreach ( $options as $option ) {
                    // This handles < 2.4.0 bw compatibility where text attributes were not sanitized.
                    $selected = sanitize_title( $args['selected'] ) === $args['selected'] ? selected( $args['selected'], sanitize_title( $option ), false ) : selected( $args['selected'], $option, false );
                    $html .= '<option value="' . esc_attr( $option ) . '" ' . $selected . '>' . esc_html( apply_filters( 'woocommerce_variation_option_name', $option ) ) . '</option>';
                }
            }
        }

        $html .= '</select>';

        echo apply_filters( 'woocommerce_dropdown_variation_attribute_options_html', $html, $args ); // WPCS: XSS ok.
    }
}

if ( ! function_exists( 'woocommerce_breadcrumb' ) ) {

    /**
     * Output the WooCommerce Breadcrumb.
     *
     * @param array $args Arguments.
     */
    function woocommerce_breadcrumb( $args = array() ) {
        $args = wp_parse_args( $args, apply_filters( 'woocommerce_breadcrumb_defaults', array(
            'delimiter'   => '',
            'wrap_before' => '<ul class="breadcrumbs">',
            'wrap_after'  => '</ul>',
            'before'      => '',
            'after'       => '',
            'home'        => _x( 'Home', 'breadcrumb', 'woocommerce' ),
        ) ) );

        $breadcrumbs = new WC_Breadcrumb();

        if ( ! empty( $args['home'] ) ) {
            $breadcrumbs->add_crumb( $args['home'], apply_filters( 'woocommerce_breadcrumb_home_url', home_url() ) );
        }

        $args['breadcrumb'] = $breadcrumbs->generate();

        /**
         * WooCommerce Breadcrumb hook
         *
         * @hooked WC_Structured_Data::generate_breadcrumblist_data() - 10
         */
        do_action( 'woocommerce_breadcrumb', $breadcrumbs, $args );

        wc_get_template( 'global/breadcrumb.php', $args );
    }
}

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 5 );

add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
  
function custom_override_checkout_fields( $fields ) {
	unset($fields['billing']['billing_last_name']);
	//unset($fields['billing']['billing_country']);
	unset($fields['billing']['billing_company']);
	//unset($fields['billing']['billing_city']);
	$fields['billing']['billing_city']['required'] = false;
	//unset($fields['billing']['billing_address_1']);
	$fields['billing']['billing_address_1']['required'] = false;
	unset($fields['billing']['billing_address_2']);
	unset($fields['billing']['billing_state']);
	unset($fields['billing']['billing_postcode']);
    return $fields;
}

function reduce_woocommerce_min_strength_requirement( $strength ) {
    return 0;
}
add_filter( 'woocommerce_min_password_strength', 'reduce_woocommerce_min_strength_requirement' );