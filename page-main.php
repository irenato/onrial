<?php
/*
Template name: main
*/
get_header('front'); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <section class="section production">
            <div class="container">
                <h2 class="section-title"><?php the_field('title1'); ?></h2>
                <div class="row">
                    <div class="col-md-5">
                        <div class="row">
                            <div class="col-md-12 col-sm-6">
                                <a href="javascript:void(0)" data-mfp-src="#prod-popup" class="popup-btn production-item" style="background-image: url(<?php the_field('products'); ?>);">
                                    <span class="production-item__name"><?php the_field('products_text'); ?></span>
                                </a>
                            </div>
                            <div class="col-md-12 col-sm-6">
                                <a href="javascript:void(0)" data-mfp-src="#prod-popup" class="popup-btn production-item" style="background-image: url(<?php the_field('products1'); ?>);">
                                    <span class="production-item__name"><?php the_field('products_text1'); ?></span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <a href="javascript:void(0)" data-mfp-src="#prod-popup" class="popup-btn production-item" style="background-image: url(<?php the_field('products2'); ?>);">
                                    <span class="production-item__name"><?php the_field('products_text2'); ?></span>
                                </a>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <a href="javascript:void(0)" data-mfp-src="#prod-popup" class="popup-btn production-item" style="background-image: url(<?php the_field('products3'); ?>);">
                                    <span class="production-item__name"><?php the_field('products_text3'); ?></span>
                                </a>
                            </div>
                            <div class="col-sm-12">
                                <a href="javascript:void(0)" data-mfp-src="#prod-popup" class="popup-btn production-item" style="background-image: url(<?php the_field('products4'); ?>);">
                                    <span class="production-item__name"><?php the_field('products_text4'); ?></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section novelties">
            <div class="container">
                <h2 class="section-title"><?php the_field('title2'); ?></h2>
                <div class="novelties-slider">
                    <div class="slide" style="background-image: url(<?php the_field('slide1'); ?>); background-color: #fff;">
                        <div class="col-md-4 col-md-offset-7 col-sm-6 col-sm-offset-6">
                            <div class="novelties-item">
                                <div class="novelties-item__name"><?php the_field('text_for_slide_1_name'); ?></div>
                                <div class="novelties-item__descr"><?php the_field('text_for_slide_1_descr'); ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="slide">
						<div class="novelties-item__img">
							<img src="<?php the_field('slide2'); ?>" alt="">
						</div>
                        <div class="col-md-4 col-md-offset-7 col-sm-6 col-sm-offset-6">
                            <div class="novelties-item">
                                <div class="novelties-item__name"><?php the_field('text_for_slide_2_name'); ?></div>
                                <div class="novelties-item__descr"><?php the_field('text_for_slide_2_name_descr'); ?> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--<section class="section popular">-->
        <!--<div class="container">-->
        <!--<h2 class="section-title">Популярное</h2>-->
        <!--<div class="popular-slider">-->
        <!--<div class="slide">-->
        <!--<a href="javascript:void(0)" class="popular-item">-->
        <!--<div class="popular-item__name">Закрепитель прикорневой с витамином С</div>-->
        <!--<img src="img/_popular-item_1.jpg" alt="" class="popular-item__img">-->
        <!--<div class="clearfix">-->
        <!--<span class="popular-item__price">150 грн. / шт</span>-->
        <!--&lt;!&ndash;<button class="btn popular-item__btn">В корзину</button>&ndash;&gt;-->
        <!--</div>-->
        <!--</a>-->
        <!--</div>-->
        <!--<div class="slide">-->
        <!--<a href="javascript:void(0)" class="popular-item">-->
        <!--<div class="popular-item__name">Закрепитель прикорневой с витамином С</div>-->
        <!--<img src="img/_popular-item_1.jpg" alt="" class="popular-item__img">-->
        <!--<div class="clearfix">-->
        <!--<span class="popular-item__price">150 грн. / шт</span>-->
        <!--&lt;!&ndash;<button class="btn popular-item__btn">В корзину</button>&ndash;&gt;-->
        <!--</div>-->
        <!--</a>-->
        <!--</div>-->
        <!--<div class="slide">-->
        <!--<a href="javascript:void(0)" class="popular-item">-->
        <!--<div class="popular-item__name">Закрепитель прикорневой с витамином С</div>-->
        <!--<img src="img/_popular-item_1.jpg" alt="" class="popular-item__img">-->
        <!--<div class="clearfix">-->
        <!--<span class="popular-item__price">150 грн. / шт</span>-->
        <!--&lt;!&ndash;<button class="btn popular-item__btn">В корзину</button>&ndash;&gt;-->
        <!--</div>-->
        <!--</a>-->
        <!--</div>-->
        <!--<div class="slide">-->
        <!--<a href="javascript:void(0)" class="popular-item">-->
        <!--<div class="popular-item__name">Закрепитель прикорневой с витамином С</div>-->
        <!--<img src="img/_popular-item_1.jpg" alt="" class="popular-item__img">-->
        <!--<div class="clearfix">-->
        <!--<span class="popular-item__price">150 грн. / шт</span>-->
        <!--&lt;!&ndash;<button class="btn popular-item__btn">В корзину</button>&ndash;&gt;-->
        <!--</div>-->
        <!--</a>-->
        <!--</div>-->
        <!--<div class="slide">-->
        <!--<a href="javascript:void(0)" class="popular-item">-->
        <!--<div class="popular-item__name">Закрепитель прикорневой с витамином С</div>-->
        <!--<img src="img/_popular-item_1.jpg" alt="" class="popular-item__img">-->
        <!--<div class="clearfix">-->
        <!--<span class="popular-item__price">150 грн. / шт</span>-->
        <!--&lt;!&ndash;<button class="btn popular-item__btn">В корзину</button>&ndash;&gt;-->
        <!--</div>-->
        <!--</a>-->
        <!--</div>-->
        <!--</div>-->
        <!--</div>-->
        <!--</section>-->

        <section class="support" style="background-image: url(<?php the_field('bg_for_3_block'); ?>);">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-6 col-md-7 col-md-offset-5">
                        <h2 class="support-title"><?php the_field('title_for_block'); ?></h2>
                        <div class="suppot-slogan">
                            <p><?php the_field('text_for_block'); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="about">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 col-lg-5">
                        <img src="<?php the_field('img_for_block_4'); ?>" alt="" class="about__banner">
                    </div>
                    <div class="col-md-7 col-lg-6">
                        <h2 class="about__title"><?php the_field('title_for_block_4'); ?></h2>
                        <div class="about__text">
                            <p><?php the_field('text_for_block_4'); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section partners">
            <div class="container">
                <h2 class="section-title"><?php the_field('title_5'); ?></h2>
                <div class="row">
                    <div class="col-md-5 col-sm-6">
                        <div class="partner-item partner-item--1" style="background-image: url(<?php the_field('blcok_5_img1'); ?>);">
                            <div>
                                <div class="partner-item__text"><?php the_field('text_for_img_1'); ?></div>
                            </div>
                        </div>
                        <div class="partner-item partner-item--2" style="background-image: url(<?php the_field('blcok_5_img2'); ?>);">
                            <div>
                                <div class="partner-item__text"><?php the_field('text_for_img_2'); ?></div>
                                <div class="partner-item__text"><?php the_field('text_for_img_22'); ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="partner-item partner-item--3" style="background-image: url(<?php the_field('blcok_5_img3'); ?>);">
                            <div>
                                <div class="partner-item__text"><?php the_field('text_for_img_3'); ?></div>
                                <div class="partner-item__text"><?php the_field('text_for_img_33'); ?></div>
                            </div>
                        </div>
                        <div class="partner-item partner-item--4" style="background-image: url(<?php the_field('blcok_5_img4'); ?>);">
                            <div>
                                <div class="partner-item__text"><?php the_field('text_for_img_4'); ?></div>
                                <div class="partner-item__text"><?php the_field('text_for_img_44'); ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div class="row">
                            <div class="col-sm-6 col-md-12">
                                <div class="partner-item partner-item--5" style="background-image: url(<?php the_field('blcok_5_img5'); ?>);">
                                    <div>
                                        <div class="partner-item__text"><?php the_field('text_for_img_5'); ?></div>
                                        <div class="partner-item__text"><?php the_field('text_for_img_55'); ?></div>
                                        <div class="partner-item__text"><?php the_field('text_for_img_555'); ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-12">
                                <div class="partner-item partner-item--6" style="background-image: url(<?php the_field('blcok_5_img6'); ?>);">
                                    <div>
                                        <div class="partner-item__text"><?php the_field('text_for_img_6'); ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section why-we">
            <div class="container">
                <h2 class="section-title section-title--white"><?php the_field('title_6'); ?></h2>
                <div class="row">
                    <div class="col-lg-5 col-lg-offset-1 col-md-6 col-sm-6">
                        <h3 class="why-we__title"><?php the_field('we_title1'); ?></h3>
                        <ul class="why-we-list hidden-xs">
                            <li class="why-we__left"><?php the_field('why-we1_text1'); ?></li>
                            <li class="why-we__left"><?php the_field('why-we1_text2'); ?></li>
                            <li class="why-we__left"><?php the_field('why-we1_text3'); ?></li>
                            <li class="why-we__left"><?php the_field('why-we1_text4'); ?></li>
                            <li class="why-we__left"><?php the_field('why-we1_text5'); ?></li>
                        </ul>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <h3 class="why-we__title"><?php the_field('why-we_title2'); ?></h3>
                        <ul class="why-we-list">
                            <li class="why-we__right">
                                <img src="<?php the_field('why-we2_img1'); ?>" alt="">
                                <span><?php the_field('why-we2_text1'); ?></span>
                            </li>
                            <li class="why-we__right">
                                <img src="<?php the_field('why-we2_img2'); ?>" alt="">
                                <span><?php the_field('why-we2_text2'); ?></span>
                            </li>
                            <li class="why-we__right">
                                <img src="<?php the_field('why-we2_img3'); ?>" alt="">
                                <span><?php the_field('why-we2_text3'); ?></span>
                            </li>
                            <li class="why-we__right">
                                <img src="<?php the_field('why-we2_img4'); ?>" alt="">
                                <span><?php the_field('why-we2_text4'); ?></span>
                            </li>
                            <li class="why-we__right">
                                <img src="<?php the_field('why-we2_img5'); ?>" alt="">
                                <span><?php the_field('why-we2_text5'); ?></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <section class="profit" style="background-image: url(<?php the_field('bg_for_block_7'); ?>);">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 col-lg-offset-1">
                        <h2 class="profit__title"><?php the_field('title_7'); ?></h2>
                        <div class="profit__descr"><?php the_field('text_for_block7'); ?></div>
                        <a href="<?php the_field('btn_link_block7'); ?>" data-mfp-src="#prod-popup" class="popup-btn btn profit__btn"><?php the_field('btn_text_block7'); ?></a>
                    </div>
                </div>
            </div>
        </section>

        <section class="section contacts">
            <div class="container-fluid">
                <h2 class="section-title"><?php the_field('title_8'); ?></h2>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="map-wrap">
                            <iframe src="<?= get_field('map1') ?>" width="100%" height="325" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="map-wrap">
                            <iframe src="<?= get_field('map2') ?>" width="100%" height="325" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main><!-- .site-main -->
</div><!-- .content-area -->
<?php get_footer(); ?>
