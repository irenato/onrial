<?php /* Template Name: inner catalog */?>

<?php

get_header(); ?>
	<main class="inner-page">
	<div class="container">
		<ul class="breadcrumbs">
			<li><a href="javascript:void(0)">Главная</a></li>
			<li><a href="javascript:void(0)">Продукция</a></li>
			<li>Ресницы</li>
		</ul>

		<h2 class="page-title">Ресницы</h2>

		<div class="row">
			<div class="col-md-2 col-sm-3">
				<aside class="catalog-sidebar">
					<div class="select-wrap">
						<select name="" id="">
							<option value="">Сортировать по</option>
							<option value="">Сортировать 1</option>
							<option value="">Сортировать 2</option>
							<option value="">Сортировать 3</option>
						</select>
					</div>
					<div class="filter-block">
						<div class="filter-block__title">Форма:</div>
						<ul class="filter-block__list">
							<li>
								<input type="checkbox" class="filter-block__check" id="filter-block__check_1" hidden>
								<label for="filter-block__check_1" class="filter-block__label">BB</label>
							</li>
							<li>
								<input type="checkbox" class="filter-block__check" id="filter-block__check_2" hidden>
								<label for="filter-block__check_2" class="filter-block__label">B</label>
							</li>
							<li>
								<input type="checkbox" class="filter-block__check" id="filter-block__check_3" hidden>
								<label for="filter-block__check_3" class="filter-block__label">CC</label>
							</li>
							<li>
								<input type="checkbox" class="filter-block__check" id="filter-block__check_4" hidden>
								<label for="filter-block__check_4" class="filter-block__label">C</label>
							</li>
						</ul>
					</div>
					<div class="filter-block">
						<div class="filter-block__title">Форма:</div>
						<ul class="filter-block__list">
							<li>
								<input type="checkbox" class="filter-block__check" id="filter-block__check_1" hidden>
								<label for="filter-block__check_1" class="filter-block__label">BB</label>
							</li>
							<li>
								<input type="checkbox" class="filter-block__check" id="filter-block__check_2" hidden>
								<label for="filter-block__check_2" class="filter-block__label">B</label>
							</li>
							<li>
								<input type="checkbox" class="filter-block__check" id="filter-block__check_3" hidden>
								<label for="filter-block__check_3" class="filter-block__label">CC</label>
							</li>
							<li>
								<input type="checkbox" class="filter-block__check" id="filter-block__check_4" hidden>
								<label for="filter-block__check_4" class="filter-block__label">C</label>
							</li>
						</ul>
					</div>
				</aside>
			</div>
			<div class="col-md-10 col-sm-9">
				<ul class="catalog-grid clearfix">
					<li>
						<a href="javascript:void(0)" class="popular-item">
							<div class="popular-item__name">Закрепитель прикорневой с витамином С</div>
							<img src=""<?php echo get_template_directory_uri(); ?>/img/_popular-item_1.jpg" alt="" class="popular-item__img">
							<div class="clearfix">
								<span class="popular-item__price">150 грн. / шт</span>
								<button class="btn popular-item__btn">В корзину</button>
							</div>
						</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="popular-item">
							<div class="popular-item__name">Закрепитель прикорневой с витамином С</div>
							<img src="<?php echo get_template_directory_uri(); ?>/img/_popular-item_1.jpg" alt="" class="popular-item__img">
							<div class="clearfix">
								<span class="popular-item__price">150 грн. / шт</span>
								<button class="btn popular-item__btn">В корзину</button>
							</div>
						</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="popular-item">
							<div class="popular-item__name">Закрепитель прикорневой с витамином С</div>
							<img src="<?php echo get_template_directory_uri(); ?>/img/_popular-item_1.jpg" alt="" class="popular-item__img">
							<div class="clearfix">
								<span class="popular-item__price">150 грн. / шт</span>
								<button class="btn popular-item__btn">В корзину</button>
							</div>
						</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="popular-item">
							<div class="popular-item__name">Закрепитель прикорневой с витамином С</div>
							<img src="<?php echo get_template_directory_uri(); ?>/img/_popular-item_1.jpg" alt="" class="popular-item__img">
							<div class="clearfix">
								<span class="popular-item__price">150 грн. / шт</span>
								<button class="btn popular-item__btn">В корзину</button>
							</div>
						</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="popular-item">
							<div class="popular-item__name">Закрепитель прикорневой с витамином С</div>
							<img src="<?php echo get_template_directory_uri(); ?>/img/_popular-item_1.jpg" alt="" class="popular-item__img">
							<div class="clearfix">
								<span class="popular-item__price">150 грн. / шт</span>
								<button class="btn popular-item__btn">В корзину</button>
							</div>
						</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="popular-item">
							<div class="popular-item__name">Закрепитель прикорневой с витамином С</div>
							<img src="<?php echo get_template_directory_uri(); ?>/img/_popular-item_1.jpg" alt="" class="popular-item__img">
							<div class="clearfix">
								<span class="popular-item__price">150 грн. / шт</span>
								<button class="btn popular-item__btn">В корзину</button>
							</div>
						</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="popular-item">
							<div class="popular-item__name">Закрепитель прикорневой с витамином С</div>
							<img src="<?php echo get_template_directory_uri(); ?>/img/_popular-item_1.jpg" alt="" class="popular-item__img">
							<div class="clearfix">
								<span class="popular-item__price">150 грн. / шт</span>
								<button class="btn popular-item__btn">В корзину</button>
							</div>
						</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="popular-item">
							<div class="popular-item__name">Закрепитель прикорневой с витамином С</div>
							<img src="<?php echo get_template_directory_uri(); ?>/img/_popular-item_1.jpg" alt="" class="popular-item__img">
							<div class="clearfix">
								<span class="popular-item__price">150 грн. / шт</span>
								<button class="btn popular-item__btn">В корзину</button>
							</div>
						</a>
					</li>
					<li>
						<a href="javascript:void(0)" class="popular-item">
							<div class="popular-item__name">Закрепитель прикорневой с витамином С</div>
							<img src="<?php echo get_template_directory_uri(); ?>/img/_popular-item_1.jpg" alt="" class="popular-item__img">
							<div class="clearfix">
								<span class="popular-item__price">150 грн. / шт</span>
								<button class="btn popular-item__btn">В корзину</button>
							</div>
						</a>
					</li>
				</ul>
				<ul class="paginator">
					<li><a href="javascript:void(0)">←</a></li>
					<li><a href="javascript:void(0)">1</a></li>
					<li><a href="javascript:void(0)">2</a></li>
					<li class="current">3</li>
					<li><a href="javascript:void(0)">4</a></li>
					<li><a href="javascript:void(0)">5</a></li>
					<li><a href="javascript:void(0)">→</a></li>
				</ul>
			</div>
		</div>
	</div>
	</main>

<?php get_footer(); ?>
